package briatore.app.db;

import briatore.estoque.entity.Entrada;
import briatore.estoque.entity.Fornecedor;
import briatore.estoque.entity.ItemEstoque;
import briatore.estoque.entity.Produto;
import briatore.estoque.repository.EntradaRepository;
import briatore.estoque.repository.FornecedorRepository;
import briatore.estoque.repository.ItemEstoqueRepository;
import briatore.estoque.repository.ProdutoRepository;
import briatore.estoque.entity.Livro;
import briatore.estoque.repository.LivroRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class CargaDeDados implements CommandLineRunner {

    private FornecedorRepository fornecedorRepository;
    private ProdutoRepository produtoRepository;
    private EntradaRepository entradaRepository;
    private ItemEstoqueRepository itemRepository;
    private final LivroRepository livroRepository;

    CargaDeDados(FornecedorRepository fornecedorRepository,
                 ProdutoRepository produtoRepository,
                 EntradaRepository entradaRepository,
                 ItemEstoqueRepository itemRepository,
                 LivroRepository livroRepository) {
        this.fornecedorRepository = fornecedorRepository;
        this.produtoRepository = produtoRepository;
        this.entradaRepository = entradaRepository;
        this.itemRepository = itemRepository;
        this.livroRepository = livroRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        Fornecedor fornecedor1 = new Fornecedor("Fornecedor 1", "Cidade 1", "Telefone 1");
        Fornecedor fornecedor2 = new Fornecedor("Fornecedor 2", "Cidade 2", "Telefone 2");
        fornecedorRepository.save(fornecedor1);
        fornecedorRepository.save(fornecedor2);

        Livro livro1 = new Livro(
                "Agile Software Development, Principles, Patterns, and Practices",
                "Agile Software Development",
                "Written by a software developer for software developers, this book is a unique collection of the latest "
                        + "software development methods. The author includes OOD, UML, Design Patterns, Agile and XP methods with "
                        + "a detailed description of a complete software design for reusable programs in C++ and Java. Using a practical, "
                        + "problem-solving approach, it shows how to develop an object-oriented application—from the early stages "
                        + "of analysis, through the low-level design and into the implementation.",
                "Robert C. Martin",
                "agile_software_development.jpg", new BigDecimal("19.30"), new BigDecimal("38.90"));
        livro1.setFornecedor(fornecedor1);

        Livro livro2 = new Livro(
                "Enterprise Integration Patterns: Designing, Building, and Deploying Messaging Solutions",
                "Enterprise Integration Patterns",
                "Enterprise Integration Patterns provides an invaluable catalog of sixty-five patterns, with "
                        + "real-world solutions that demonstrate the formidable of messaging and help you to design "
                        + "effective messaging solutions for your enterprise.",
                "Gregor Hohpe",
                "enterprise_integration.jpg", new BigDecimal("29.90"), new BigDecimal("69.90"));
        livro2.setFornecedor(fornecedor2);

        livroRepository.save(livro1);
        livroRepository.save(livro2);
        //produtoRepository.save(livro1);
        //produtoRepository.save(livro2);

        Entrada entrada1 = new Entrada();
        entrada1.setQuantidade(10);
        entrada1.setPrecoUnidade(new BigDecimal(19.30));
        ItemEstoque item1 = entrada1.getItem();
        item1.setProduto(livro1);
        item1.setQtdeArmazenada(entrada1.getQuantidade());
        item1.setPreco(entrada1.getPrecoUnidade());
        itemRepository.save(item1);
        entrada1.setItem(item1);
        entradaRepository.save(entrada1);

        Entrada entrada2 = new Entrada();
        entrada2.setQuantidade(20);
        entrada2.setPrecoUnidade(new BigDecimal(69.90));
        ItemEstoque item2 = entrada2.getItem();
        item2.setProduto(livro2);
        item2.setQtdeArmazenada(entrada2.getQuantidade());
        item2.setPreco(entrada2.getPrecoUnidade());
        itemRepository.save(item2);
        entrada2.setItem(item2);
        entradaRepository.save(entrada2);
    }
}
