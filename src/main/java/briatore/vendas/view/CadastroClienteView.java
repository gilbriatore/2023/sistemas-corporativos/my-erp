package briatore.vendas.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CadastroClienteView {
	@GetMapping("/vendas/clientes")
	public String navegarPara() {
		return "/vendas/cadastro-cliente";
	}
}
