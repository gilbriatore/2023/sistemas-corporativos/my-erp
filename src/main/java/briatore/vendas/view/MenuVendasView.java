package briatore.vendas.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MenuVendasView {
	@GetMapping("/vendas")
	public String navegarPara() {
		return "/vendas/menu-vendas";
	}
}
