package briatore.vendas.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CadastrosVendasView {
	@GetMapping("/vendas/cadastros")
	public String navegarPara() {
		return "/vendas/cadastros-vendas";
	}
}
