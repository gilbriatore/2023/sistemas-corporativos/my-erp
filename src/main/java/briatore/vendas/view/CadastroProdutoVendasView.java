package briatore.vendas.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CadastroProdutoVendasView {
	@GetMapping("/vendas/produtos")
	public String navegarPara() {
		return "/vendas/cadastro-produto";
	}
}
