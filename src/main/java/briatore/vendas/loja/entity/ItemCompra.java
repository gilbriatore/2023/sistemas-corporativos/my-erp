package briatore.vendas.loja.entity;

import briatore.estoque.entity.ItemEstoque;
import briatore.estoque.entity.Livro;
import jakarta.persistence.*;

import java.math.BigDecimal;
@Entity
public class ItemCompra {

	@Id
	@GeneratedValue
	private Long id;
	private Formato formato;
	private Integer quantidade;
	@ManyToOne
	private ItemEstoque itemEstoque;
	@Transient
	private Livro livro;

	public ItemCompra() {
		popularLivro();
	}
	public ItemCompra(ItemEstoque itemEstoque, Formato formato) {
		this.itemEstoque = itemEstoque;
		this.formato = formato;
		this.quantidade = 1;
		popularLivro();
	}

	private void popularLivro(){
		if (itemEstoque != null && itemEstoque.getProduto() instanceof Livro) {
			this.livro = (Livro) itemEstoque.getProduto();
		}
	}

	public ItemEstoque getItemEstoque() {
		return itemEstoque;
	}

	public void incrementaQuantidade(Integer quantidade) {
		this.quantidade += quantidade;
	}

	public boolean isImpresso() {
		return formato.equals(Formato.IMPRESSO);
	}

	public String getImagem() {
		return this.livro.getImagem();
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public BigDecimal getValorUnico() {
		return this.livro.getValor(formato);
	}

	public String getTitulo() {
		return this.livro.getTitulo();
	}

	public String getTituloCurto() {
		return this.livro.getTituloCurto();
	}

	public String getCodigo() {
		return this.livro.getCodigo();
	}

	public BigDecimal getTotal() {
		BigDecimal valorLivro = this.getValorUnico();
		return valorLivro.multiply(new BigDecimal(this.quantidade));
	}

	public Formato getFormato() {
		return formato;
	}

	public boolean temCodigo(String codigo) {
		return this.getCodigo().equals(codigo);
	}

	@Override
	public String toString() {
		return "ItemCompra [titulo=" + this.livro.getTitulo() + ", image=" + this.livro.getImagem()
				+ ", codigo=" + this.livro.getCodigo()
				+ ", formato=" + formato + ", quantidade=" + quantidade
				+ ", valorUnico=" + livro.getValor(formato) + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.livro.getCodigo() == null) ? 0 : this.livro.getCodigo().hashCode());
		result = prime * result + ((formato == null) ? 0 : formato.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemCompra other = (ItemCompra) obj;
		if (this.livro.getCodigo() == null) {
			if (other.livro.getCodigo() != null)
				return false;
		} else if (!this.livro.getCodigo().equals(other.livro.getCodigo()))
			return false;
		if (formato != other.formato)
			return false;
		return true;
	}
}
