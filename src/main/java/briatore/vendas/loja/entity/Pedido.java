package briatore.vendas.loja.entity;

import briatore.financeiro.entity.Pagamento;
import jakarta.persistence.*;

import java.time.LocalDate;
import java.util.Set;


@Entity
public class Pedido {

	@Id @GeneratedValue
	private Long id;

	private LocalDate data;
	
	@OneToMany(cascade=CascadeType.PERSIST)
	private Set<ItemCompra> itens;

	@OneToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(unique=true)
	private Pagamento pagamento;

	public Pedido() {
		this.data = LocalDate.now();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setItens(Set<ItemCompra> itens) {
		this.itens = itens;
	}

	public Set<ItemCompra> getItens() {
		return itens;
	}

	public String getFormato() {
		return this.temApenasLivrosImpressos() ? "impresso" : "ebook";
	}
	
	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

	private boolean temApenasLivrosImpressos() {
		
		for (ItemCompra itemCompra : this.itens) {
			if(!itemCompra.isImpresso()) {
				return false;
			}
		}
		return true;
	}

	public void setPagamento(Pagamento pagamento) {
		this.pagamento = pagamento;
	}

	public String getStatus() {
		return this.pagamento == null ? "INDEFINIDO" : this.pagamento.getStatus();
	}


	@Override
	public String toString() {
		return "Pedido [id=" + id + ", itens=" + itens + ", pagamento=" + pagamento + "]";
	}
}
