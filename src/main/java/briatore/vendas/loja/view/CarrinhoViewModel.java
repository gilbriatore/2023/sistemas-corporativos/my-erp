package briatore.vendas.loja.view;

import briatore.estoque.entity.ItemEstoque;
import briatore.estoque.service.EstoqueService;
import briatore.vendas.loja.service.CarrinhoService;
import briatore.vendas.loja.entity.Formato;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@RequestMapping("/vendas")
@Scope("request")
public class CarrinhoViewModel {

    CarrinhoService carrinhoService;
    EstoqueService estoqueService;

    CarrinhoViewModel(CarrinhoService carrinhoService,
                      EstoqueService estoqueService) {
        this.carrinhoService = carrinhoService;
        this.estoqueService = estoqueService;
    }

    @RequestMapping("/carrinho")
    public String mostrar(RedirectAttributes model) {
        if (this.carrinhoService.getItensCompra().isEmpty()) {
            model.addFlashAttribute("messageInfo", "Carrinho de compras está vazio!");
        }
        return "/vendas/loja/carrinho";
    }

    @RequestMapping("/carrinho/adicionar")
    public String adicionarItem(@RequestParam("id") Long id,
                                @RequestParam("formatoLivro") Formato formato) {

        ItemEstoque itemEstoque = this.estoqueService.reservarItem(id);
        this.carrinhoService.adicionarItem(itemEstoque, formato);

        return "redirect:/vendas/carrinho";
    }


    @RequestMapping("/carrinho/remover")
    public String removerItem(@RequestParam("codigo") String codigo,
                              @RequestParam("formato") Formato formato,
                              RedirectAttributes model) {

        ItemEstoque item = this.carrinhoService.removerItem(codigo, formato);
        this.estoqueService.estornarItem(item.getId());
        model.addFlashAttribute("messageInfo", "O item foi removido com sucesso.");
        return "redirect:/vendas/carrinho";
    }

    @RequestMapping("/carrinho/calcularFrete")
    public String calcularFrete(@RequestParam("cepDestino") String cepDestino, RedirectAttributes model) {
        this.carrinhoService.calcularFrete(cepDestino);
        return "redirect:/vendas/carrinho";
    }

    @RequestMapping("/carrinho/finalizar")
    public String finalizar(@RequestParam("cartao") String cartao,
                            @RequestParam("titular") String titular,
                            RedirectAttributes model) {

        if (!this.carrinhoService.isFreteCalculado()) {
            model.addFlashAttribute("messageWarn", "O frete deve ser calculado antes de finalizar o pedido!");
            return "redirect:/vendas/carrinho";
        }

        this.carrinhoService.setNumeroCartao(cartao);
        this.carrinhoService.setTitularCartao(titular);

        if (!this.carrinhoService.isCartaoPreenchido()) {
            model.addFlashAttribute("messageWarn", "Por favor preencha os dados do cartão!");
            return "redirect:/vendas/carrinho";
        }

        this.carrinhoService.processarPagamento();

        if (!this.carrinhoService.isPagamentoProcessado()) {
            model.addFlashAttribute("messageWarn", "O pagamento não pode ser processado!");
            return "redirect:/vendas/carrinho";
        }

        this.carrinhoService.finalizarPedido();

        model.addFlashAttribute("messageInfo", "Pedido realizado com sucesso!");

        return "redirect:/vendas/carrinho";
    }
}