package briatore.vendas.loja.view;

import java.util.List;

import briatore.estoque.entity.ItemEstoque;
import briatore.estoque.service.EstoqueService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/vendas")
public class LojaViewModel {

	private EstoqueService estoqueService;

	LojaViewModel(EstoqueService estoqueService) {
		this.estoqueService = estoqueService;
	}

	@RequestMapping("/loja")
	public String navegarParaIndex(Model model) {
		List<ItemEstoque> itens = estoqueService.listarItens();
		model.addAttribute("listaItens", itens);
		return "/vendas/loja/index";
	}

	@RequestMapping("/loja/livro/{id}")
	public String verLivro(@PathVariable("id") Long id, Model model) {
		ItemEstoque item = estoqueService.buscarPorId(id);
		model.addAttribute("item", item);
		return "/vendas/loja/livro";
	}
}
