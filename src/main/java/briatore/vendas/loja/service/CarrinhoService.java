package briatore.vendas.loja.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import briatore.estoque.entity.ItemEstoque;
import briatore.financeiro.entity.Pagamento;
import briatore.financeiro.service.PagamentoService;
import briatore.vendas.loja.entity.Formato;
import briatore.vendas.loja.entity.ItemCompra;
import briatore.vendas.loja.entity.Pedido;
import briatore.vendas.loja.repository.PedidoRepository;
import jakarta.servlet.http.HttpSession;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class CarrinhoService {
	private Set<ItemCompra> itensDeCompra = new LinkedHashSet<>();
	private BigDecimal valorFrete = BigDecimal.ZERO;
	private String cepDestino;
	private String numeroCartao;
	private String titularCartao;
	private Pagamento pagamento;
	private PagamentoService pagamentoService;
	private PedidoRepository pedidoRepository;

	CarrinhoService(HttpSession session,
					PagamentoService pagamentoService,
					PedidoRepository pedidoRepository) {
		session.setAttribute("carrinho", this);
		this.pagamentoService = pagamentoService;
		this.pedidoRepository = pedidoRepository;
	}

	public void adicionarItem(ItemEstoque itemEstoque, Formato formato) {

		ItemCompra itemCompra = new ItemCompra(itemEstoque, formato);

		if (jaExisteItem(itemCompra)) {
			ItemCompra itemCarrinho = this.procurarItem(itemCompra);
			itemCarrinho.incrementaQuantidade(itemCompra.getQuantidade());
		} else {
			this.itensDeCompra.add(itemCompra);
		}
	}

	public ItemEstoque removerItem(String codigo, Formato formato) {

		ItemCompra itemARemover = buscarItemPorCodigo(codigo, formato);

		if (itemARemover != null) {
			this.itensDeCompra.remove(itemARemover);
			if (itemARemover.isImpresso()) {
				this.valorFrete = BigDecimal.ZERO;
			}
		}

		cancelarPagamento();
		return itemARemover.getItemEstoque();
	}

	public void processarPagamento() {
		this.pagamento = this.pagamentoService.processarPagamento(
				this.titularCartao,
				this.numeroCartao,
				this.getTotal());
	}

	private void cancelarPagamento() {
		if (this.pagamento != null) {
			this.pagamentoService.cancelarPagamento(this.pagamento);
			this.pagamento = null;
		}
	}

	/** Usar o sistema de pedidos restful do https://spring.io/guides/tutorials/rest/ */
	public void finalizarPedido() {

		//TODO: Enviar pedido por email
		Pedido pedido = new Pedido();
		pedido.setItens(new LinkedHashSet<>(this.itensDeCompra));
		pedido.setPagamento(pagamento);
		this.pedidoRepository.save(pedido);

		this.limparCarrinho();
	}

	public void calcularFrete(final String novoCepDestino) {

		//TODO: Implementar a chamada do WS para calcular o frete
		this.cepDestino = novoCepDestino;
		this.valorFrete = new BigDecimal(30);

		//WsCorreios wsCorreios = new WsCorreios();
		//this.valorFrete = wsCorreios.calcularFrete(novoCepDestino);

		//https://www.devmedia.com.br/calculo-de-frete-com-os-correios/25711

		//https://igorjoaquim-pg.medium.com/consumindo-webservice-dos-correios-com-spring-boot-154e481ff1a8
		//https://spring.io/guides/gs/consuming-web-service/

		//https://www.cepcerto.com/webservice-cep
		//https://viacep.com.br/ws/01001000/json/

		//https://www.cepcerto.com/webservice-calculo-frete

		//https://www.guiadoexcel.com.br/calculo-de-prazo-de-entrega-e-valores-dos-correios-excel-vba/
	}

	public String getCepDestino() {
		return cepDestino;
	}

	public List<ItemCompra> getItensCompra() {
		return new ArrayList<ItemCompra>(this.itensDeCompra);
	}

	public BigDecimal getTotal() {

		BigDecimal total = BigDecimal.ZERO;

		for (ItemCompra item : this.itensDeCompra) {
			total = total.add(item.getTotal());
		}

		return total.add(valorFrete);
	}

	public Pagamento getPagamento() {
		return this.pagamento;
	}

	public BigDecimal getValorFrete() {
		return valorFrete;
	}

	public boolean isFreteCalculado() {
		return this.valorFrete.compareTo(BigDecimal.ZERO) > 0;
	}

	public boolean isPagamentoProcessado() {
		if (this.pagamento == null) {
			return false;
		}
		return this.pagamento.isProcessado();
	}

	public boolean isProntoParaSerFinalizado() {
		return this.isFreteCalculado() && this.isPagamentoProcessado();
	}

	public boolean possuiLivrosImpressos() {

		for (ItemCompra itemCompra : this.itensDeCompra) {
			if (itemCompra.isImpresso()) {
				return true;
			}
		}
		return false;
	}

	private void limparCarrinho() {
		this.itensDeCompra = new LinkedHashSet<>();
		this.valorFrete = BigDecimal.ZERO;
		this.numeroCartao = "";
		this.titularCartao = "";
		this.cepDestino = "";
	}

	private boolean jaExisteItem(final ItemCompra item) {
		return this.itensDeCompra.contains(item);
	}

	private ItemCompra procurarItem(final ItemCompra itemProcurado) {
		for (ItemCompra item : this.itensDeCompra) {
			if (item.equals(itemProcurado)) {
				return item;
			}
		}
		throw new IllegalStateException("Item não encontrado");
	}

	private ItemCompra buscarItemPorCodigo(final String codigo, Formato formato) {

		for (ItemCompra item : this.itensDeCompra) {
			if (item.getCodigo().equals(codigo) && item.getFormato().equals(formato)) {
				return item;
			}
		}

		return null;
	}

	@SuppressWarnings("unused")
	private List<String> getCodigosDosItensImpressos() {
		List<String> codigos = new ArrayList<>();

		for (ItemCompra item : this.itensDeCompra) {
			if (item.isImpresso())
				codigos.add(item.getCodigo());
		}
		return codigos;
	}

	public String getNumeroCartao() {
		return numeroCartao;
	}

	public void setNumeroCartao(String numeroCartao) {
		this.numeroCartao = numeroCartao;
	}

	public String getTitularCartao() {
		return titularCartao;
	}

	public void setTitularCartao(String titularCartao) {
		this.titularCartao = titularCartao;
	}

	public boolean isCartaoPreenchido() {
		return isNotEmptyString(numeroCartao)  && isNotEmptyString(titularCartao);
	}

	private boolean isNotEmptyString(String string) {
		return string != null && !string.trim().isEmpty();
	}
}
