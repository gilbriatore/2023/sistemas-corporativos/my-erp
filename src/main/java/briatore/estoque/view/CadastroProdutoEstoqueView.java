package briatore.estoque.view;

import briatore.estoque.entity.Fornecedor;
import briatore.estoque.entity.Produto;
import briatore.estoque.repository.FornecedorRepository;
import briatore.estoque.repository.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
import java.util.Optional;

@Controller
public class CadastroProdutoEstoqueView {
	@Autowired
	ProdutoRepository produtoRepository;
	@Autowired
	FornecedorRepository fornecedorRepository;

	@GetMapping("/estoque/produto/listar")
	public String navegarParaListar(Model model) {
		List<Produto> listaProdutos = produtoRepository.findAll();
		model.addAttribute("listaProdutos", listaProdutos);
		return "/estoque/produto/listar-produtos";
	}

	@GetMapping("/estoque/produto/novo")
	public String navegarParaNovo(Model model) {

		Produto produto = new Produto();
		model.addAttribute("produto", produto);

		List<Fornecedor> listaFornecedores = fornecedorRepository.findAll();
		model.addAttribute("listaFornecedores", listaFornecedores);

		return "/estoque/produto/incluir-produto";
	}


	@GetMapping("/estoque/produto/editar/{id}")
	public String navegarParaEditar(@PathVariable("id") Long id, Model model) {

		Optional<Produto> produto = produtoRepository.findById(id);
		model.addAttribute("produto", produto.get());

		List<Fornecedor> listaFornecedores = fornecedorRepository.findAll();
		model.addAttribute("listaFornecedores", listaFornecedores);

		return "/estoque/produto/editar-produto";
	}

	@PostMapping("/estoque/produto/salvar")
	public String salvar(@ModelAttribute("produto") Produto produto) {
		produtoRepository.save(produto);
		return "redirect:/estoque/produto/listar";
	}


	@GetMapping("/estoque/produto/excluir/{id}")
	public String excluir(@PathVariable("id") Long id) {
		produtoRepository.deleteById(id);
		return "redirect:/estoque/produto/listar";
	}
}
