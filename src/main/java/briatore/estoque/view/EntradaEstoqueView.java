package briatore.estoque.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class EntradaEstoqueView {
	@GetMapping("/estoque/entrada")
	public String navegarPara() {
		return "/estoque/entrada-estoque";
	}
}
