package briatore.estoque.view;

import briatore.estoque.entity.Fornecedor;
import briatore.estoque.repository.FornecedorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class CadastroFornecedorView {

	@Autowired
	FornecedorRepository repository;

	@GetMapping("/estoque/fornecedor/listar")
	public String navegarParaListar(Model model) {
		List<Fornecedor> fornecedores = repository.findAll();
		model.addAttribute("fornecedores", fornecedores);
		return "/estoque/fornecedor/listar-fornecedores";
	}

	@GetMapping("/estoque/fornecedor/novo")
	public String navegarParaNovo(Model model) {

		Fornecedor fornecedor = new Fornecedor();
		model.addAttribute("fornecedor", fornecedor);

		List<String> listaCidades = this.getListaCidades();
		model.addAttribute("listaCidades", listaCidades);

		return "/estoque/fornecedor/incluir-fornecedor";
	}

	@GetMapping("/estoque/fornecedor/editar/{id}")
	public String navegarParaEditar(@ModelAttribute("id") Long id, Model model)  {

		Optional<Fornecedor> fornecedor = repository.findById(id);
		model.addAttribute("fornecedor", fornecedor.get());

		List<String> listaCidades = this.getListaCidades();
		model.addAttribute("listaCidades", listaCidades);

		return "/estoque/fornecedor/editar-fornecedor";
	}

	@PostMapping("/estoque/fornecedor/salvar")
	public String salvar(@ModelAttribute("fornecedor") Fornecedor fornecedor) {
		repository.save(fornecedor);
		return "redirect:/estoque/fornecedor/listar";
	}

	@GetMapping("/estoque/fornecedor/excluir/{id}")
	public String excluir(@ModelAttribute("id") Long id) {
		repository.deleteById(id);
		return "redirect:/estoque/fornecedor/listar";
	}
	private List<String> getListaCidades() {
		List<String> listaCidades = new ArrayList<>();
		listaCidades.add("Curitiba, PR");
		listaCidades.add("Recife, PE");
		listaCidades.add("Rio de Janeiro, RJ");
		listaCidades.add("São Paulo, SP");
		return listaCidades;
	}
}
