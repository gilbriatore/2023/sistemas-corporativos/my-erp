package briatore.estoque.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class InventarioView {
	@GetMapping("/estoque/inventario")
	public String navegarPara() {
		return "/estoque/inventario";
	}
}
