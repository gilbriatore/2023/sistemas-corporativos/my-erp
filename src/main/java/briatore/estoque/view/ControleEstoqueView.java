package briatore.estoque.view;

import briatore.estoque.entity.Entrada;
import briatore.estoque.entity.ItemEstoque;
import briatore.estoque.entity.Produto;
import briatore.estoque.entity.Saida;
import briatore.estoque.repository.EntradaRepository;
import briatore.estoque.repository.ItemEstoqueRepository;
import briatore.estoque.repository.ProdutoRepository;
import briatore.estoque.repository.SaidaRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class ControleEstoqueView {

	ItemEstoqueRepository itemRepository;
	ProdutoRepository produtoRepository;
	EntradaRepository entradaRepository;
	SaidaRepository saidaRepository;

	ControleEstoqueView(ItemEstoqueRepository itemRepository,
                        ProdutoRepository produtoRepository,
                        EntradaRepository entradaRepository,
						SaidaRepository saidaRepository) {
		this.itemRepository = itemRepository;
		this.produtoRepository = produtoRepository;
		this.entradaRepository = entradaRepository;
		this.saidaRepository = saidaRepository;
	}

	@GetMapping("/estoque/controle")
	public String navegarPara(Model model) {
		List<ItemEstoque> listaItens = itemRepository.findAll();
		model.addAttribute("listaItens", listaItens);
		return "/estoque/controle-estoque";
	}

	@GetMapping("/estoque/cadastros")
	public String navegarParaCadastros() {
		return "/estoque/cadastros-estoque";
	}

	@GetMapping("/estoque/entrada/nova")
	public String navegarParaNovaEntrada(Model model) {

		Entrada entrada = new Entrada();
		model.addAttribute("entrada", entrada);

		List<Produto> listaProdutos = produtoRepository.findAll();
		model.addAttribute("listaProdutos", listaProdutos);
		return "/estoque/entrada-estoque";
	}

	@GetMapping("/estoque/entrada/excluir/{id}")
	public String excluirEntrada(@PathVariable("id") Long id) {
		Entrada entrada = entradaRepository.findById(id).get();
		ItemEstoque item = entrada.getItem();
		entradaRepository.delete(entrada);
		itemRepository.delete(item);
		return "redirect:/estoque/controle";
	}

	@PostMapping("/estoque/entrada/salvar")
	public String salvarEntrada(@ModelAttribute("entrada") Entrada entrada) {
		ItemEstoque item = entrada.getItem();
		item.setQtdeArmazenada(entrada.getQuantidade());
		item.setPreco(entrada.getPrecoUnidade());
		itemRepository.save(item);
		entrada.setItem(item);
		entradaRepository.save(entrada);
		return "redirect:/estoque/controle";
	}

	@GetMapping("/estoque/saida/baixar/{id}")
	public String navegarParaBaixarEstoque(@PathVariable("id") Long id, Model model) {

		ItemEstoque itemEstoque = itemRepository.findById(id).get();
		Saida saida = new Saida(itemEstoque, itemEstoque.getPreco());
		model.addAttribute("saida", saida);

		List<Produto> listaProdutos = produtoRepository.findAll();
		model.addAttribute("listaProdutos", listaProdutos);
		return "/estoque/baixar-estoque";
	}

	@PostMapping("/estoque/saida/salvar")
	public String salvarSaida(@RequestParam("itemId") Long itemId, @ModelAttribute("saida") Saida saida) {
		ItemEstoque itemEstoque = itemRepository.findById(itemId).get();
		itemEstoque.baixar(saida.getQtdeBaixar(), saida.getItemReservado());
		itemRepository.save(itemEstoque);
		saida.setItemEstoque(itemEstoque);
		saidaRepository.save(saida);
		return "redirect:/estoque/controle";
	}
}
