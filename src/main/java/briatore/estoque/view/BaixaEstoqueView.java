package briatore.estoque.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class BaixaEstoqueView {
	@GetMapping("/estoque/baixa")
	public String navegarPara() {
		return "baixar-estoque";
	}
}
