package briatore.estoque.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MenuEstoqueView {
	@GetMapping("/estoque")
	public String navegarPara() {
		return "/estoque/menu-estoque";
	}
}
