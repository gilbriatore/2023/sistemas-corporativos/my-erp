package briatore.estoque.api;

import briatore.estoque.entity.ItemEstoque;
import briatore.estoque.service.EstoqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/estoque")
public class EstoqueController {

    @Autowired
    private EstoqueService controle;

    @GetMapping("/itens")
    public List<ItemEstoque> listarItens(){
        return controle.listarItens();
    }

    @GetMapping("/itens/{id}")
    public ItemEstoque getItem(@PathVariable Long id){
        //return controle.listarItens().stream().filter(item -> item.getId().equals(id)).findFirst().get();
        ItemEstoque item = new ItemEstoque();
        return item;
    }

    @PostMapping("/itens")
    public ItemEstoque salvarItem(@RequestBody ItemEstoque item) {
        System.out.println(item);
        return controle.salvarItem(item);
    }

}
