package briatore.estoque.entity;

import jakarta.persistence.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity(name = "Entrada")
public class Entrada {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private ItemEstoque item;
    private Integer quantidade;
    private BigDecimal precoUnidade;
    private LocalDateTime data;

    public Entrada() {
        this.item = new ItemEstoque();
    }

    public Entrada(ItemEstoque item,
                   Integer quantidade,
                   BigDecimal precoUnidade,
                   LocalDateTime data) {
        this.item = item;
        this.quantidade = quantidade;
        this.precoUnidade = precoUnidade;
        this.data = data;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ItemEstoque getItem() {
        return item;
    }

    public void setItem(ItemEstoque item) {
        this.item = item;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public BigDecimal getPrecoUnidade() {
        return precoUnidade;
    }

    public void setPrecoUnidade(BigDecimal precoUnidade) {
        this.precoUnidade = precoUnidade;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Entrada{" +
                "id=" + id +
                ", item=" + item +
                ", quantidade=" + quantidade +
                ", precoUnitario=" + precoUnidade +
                ", data=" + data +
                '}';
    }
}
