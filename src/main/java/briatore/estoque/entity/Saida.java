package briatore.estoque.entity;

import jakarta.persistence.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
public class Saida {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne
	private ItemEstoque itemEstoque;
	private Integer qtdeBaixar;
	private BigDecimal precoDoItem;
	private Boolean isItemReservado;
	private LocalDateTime data;

	public Saida() {
		this.data = LocalDateTime.now();
		this.isItemReservado = false;
		this.qtdeBaixar = 1;
	}

	public Saida(ItemEstoque itemEstoque, BigDecimal precoDoItem){
		this();
		this.itemEstoque = itemEstoque;
		this.precoDoItem = precoDoItem;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ItemEstoque getItemEstoque() {
		return itemEstoque;
	}

	public void setItemEstoque(ItemEstoque item) {
		this.itemEstoque = item;
	}

	public Integer getQtdeBaixar() {
		return qtdeBaixar;
	}

	public void setQtdeBaixar(Integer quantidade) {
		this.qtdeBaixar = quantidade;
	}

	public BigDecimal getPrecoDoItem() {
		return precoDoItem;
	}

	public void setPrecoDoItem(BigDecimal precoDoItem) {
		this.precoDoItem = precoDoItem;
	}

	public LocalDateTime getData() {
		return data;
	}

	public void setData(LocalDateTime data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Saida{" +
				"id=" + id +
				", item=" + itemEstoque +
				", quantidade=" + qtdeBaixar +
				", precoDoItem=" + precoDoItem +
				", data=" + data +
				'}';
	}

	public Boolean getItemReservado() {
		return isItemReservado;
	}

	public void setItemReservado(Boolean itemReservado) {
		isItemReservado = itemReservado;
	}
}
