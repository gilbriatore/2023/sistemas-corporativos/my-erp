package briatore.estoque.entity;

import jakarta.persistence.*;
import java.time.LocalDateTime;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Produto {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String nome;
	@Column(length=1000)
	private String descricao;
	@ManyToOne
	private Fornecedor fornecedor;
	private String marca;
	private Integer qtdeMinima;
	private Integer qtdeMaxima;
	private LocalDateTime criadoEm;

	public Produto() {
		this.qtdeMinima = 0;
		this.qtdeMaxima = 0;
		this.criadoEm = LocalDateTime.now();
	}

	public Produto(String nome,
				   String descricao) {
		this();
		this.nome = nome;
		this.descricao = descricao;
	}
	public Produto(String nome,
				   String descricao,
				   Fornecedor fornecedor,
				   String marca) {
		this();
		this.nome = nome;
		this.descricao = descricao;
		this.fornecedor = fornecedor;
		this.marca = marca;

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public Integer getQtdeMinima() {
		return qtdeMinima;
	}

	public void setQtdeMinima(Integer qtdeMinima) {
		this.qtdeMinima = qtdeMinima;
	}

	public Integer getQtdeMaxima() {
		return qtdeMaxima;
	}

	public void setQtdeMaxima(Integer qtdeMaxima) {
		this.qtdeMaxima = qtdeMaxima;
	}

	public LocalDateTime getCriadoEm() {
		return criadoEm;
	}

	public void setCriadoEm(LocalDateTime criadoEm) {
		this.criadoEm = criadoEm;
	}

	@Override
	public String toString() {
		return "Produto{" +
				"id=" + id +
				", nome='" + nome + '\'' +
				", descricao='" + descricao + '\'' +
				", fornecedor=" + fornecedor +
				", marca='" + marca + '\'' +
				", qtdeMinima=" + qtdeMinima +
				", qtdeMaxima=" + qtdeMaxima +
				", criadoEm=" + criadoEm +
				'}';
	}
}
