package briatore.estoque.entity;

import briatore.vendas.loja.entity.Formato;
import jakarta.persistence.*;

import java.math.BigDecimal;
import java.util.UUID;


@Entity
public class Livro extends Produto {

	private String codigo;
	private String titulo;
	private String tituloCurto;
	private String nomeAutor;
	private String imagem;
	private BigDecimal valorEbook;
	private BigDecimal valorImpresso;

	public Livro() {}
	public Livro(String titulo, String tituloCurto, String descricao,
			String nomeAutor, String imagem, BigDecimal valorEbook,
			BigDecimal valorImpresso) {
		super(titulo, descricao);
		this.codigo = UUID.randomUUID().toString();
		this.titulo = titulo;
		this.tituloCurto = tituloCurto;
		this.nomeAutor = nomeAutor;
		this.imagem = imagem;
		this.valorEbook = valorEbook;
		this.valorImpresso = valorImpresso;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getTitulo() {
		return titulo;
	}

	public String getTituloCurto() {
		return tituloCurto;
	}


	public String getNomeAutor() {
		return nomeAutor;
	}

	public String getImagem() {
		return imagem;
	}

	public BigDecimal getValorEbook() {
		return valorEbook;
	}

	public BigDecimal getValorImpresso() {
		return valorImpresso;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public void setTituloCurto(String tituloCurto) {
		this.tituloCurto = tituloCurto;
	}


	public void setNomeAutor(String nomeAutor) {
		this.nomeAutor = nomeAutor;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

	public void setValorEbook(BigDecimal valorEbook) {
		this.valorEbook = valorEbook;
	}

	public void setValorImpresso(BigDecimal valorImpresso) {
		this.valorImpresso = valorImpresso;
	}

	public BigDecimal getValor(Formato formato) {
		if(formato.equals(Formato.EBOOK)) {
			return this.valorEbook;
		}
		return this.valorImpresso;
	}

	@Override
	public String toString() {
		return "Livro [codigo=" + codigo + ", titulo=" + titulo + ", tituloCurto="
				+ tituloCurto + ", nomeAutor=" + nomeAutor + ", imagem=" + imagem + ", valorEbook="
				+ valorEbook + ", valorImpresso=" + valorImpresso + "]";
	}
}
