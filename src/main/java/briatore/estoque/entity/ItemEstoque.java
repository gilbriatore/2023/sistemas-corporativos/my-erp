package briatore.estoque.entity;

import jakarta.persistence.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
public class ItemEstoque {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String lote;
    private LocalDateTime criadoEm;
    private Integer qtdeArmazenada;
    private Integer qtdeReservada;
    private BigDecimal preco;
    @ManyToOne
    private Produto produto;

    public ItemEstoque() {
        this.lote = UUID.randomUUID().toString();
        this.criadoEm = LocalDateTime.now();
        this.preco = BigDecimal.ZERO;
        this.qtdeArmazenada = 0;
        this.qtdeReservada = 0;
    }

    public Livro getLivro() {
        if (produto != null && produto instanceof Livro) {
            return (Livro) produto;
        }
        return null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public BigDecimal getPreco() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco = preco;
    }

    public LocalDateTime getCriadoEm() {
        return criadoEm;
    }

    public void setCriadoEm(LocalDateTime criadoEm) {
        this.criadoEm = criadoEm;
    }


    public Integer getQuantidade() {
        return qtdeArmazenada - qtdeReservada;
    }

    public Integer getQtdeArmazenada() {
        return qtdeArmazenada;
    }

    public void setQtdeArmazenada(Integer quantidade) {
        this.qtdeArmazenada = quantidade;
    }

    public Integer getQtdeReservada() {
        return qtdeReservada;
    }


    public void baixar(Integer qtdeBaixar, Boolean itemReservado) {
        if (itemReservado) {
            qtdeReservada -= qtdeBaixar;
        }
        qtdeArmazenada -= qtdeBaixar;
    }

    public void setQtdeReservada(Integer quantidade) {
        this.qtdeReservada = quantidade;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", lote='" + lote + '\'' +
                ", produto=" + produto +
                ", quantidade=" + qtdeArmazenada +
                ", preco=" + preco +
                ", criadoEm=" + criadoEm +
                '}';
    }
}
