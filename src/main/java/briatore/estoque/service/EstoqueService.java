package briatore.estoque.service;

import briatore.estoque.entity.Entrada;
import briatore.estoque.entity.ItemEstoque;
import briatore.estoque.entity.Saida;
import briatore.estoque.repository.ItemEstoqueRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class EstoqueService {
    private ItemEstoqueRepository repository;

    private List<Entrada> entradas= new ArrayList<>();

    private List<Saida> saidas= new ArrayList<>();

    public EstoqueService(ItemEstoqueRepository repository) {
        this.repository = repository;
    }

    public ItemEstoque salvarItem(ItemEstoque item){
        return repository.save(item);
    }

    public List<ItemEstoque> listarItens() {
        return repository.findAll();
    }
    public ItemEstoque buscarPorId(Long id) {
        return repository.findById(id).get();
    }
    public ItemEstoque reservarItem(Long id) {
        Optional<ItemEstoque> item = this.repository.findById(id);
        if (item.isPresent()) {
            item.get().setQtdeReservada(item.get().getQtdeReservada() + 1);
            this.repository.save(item.get());
        }
        return item.get();
    }

    public void estornarItem(Long id) {
        Optional<ItemEstoque> item = this.repository.findById(id);
        if (item.isPresent()) {
            item.get().setQtdeReservada(item.get().getQtdeReservada() - 1);
            this.repository.save(item.get());
        }
    }

    public void adicionarItem(Entrada entrada){
        this.entradas.add(entrada);
    }

    public void baixarItem(Saida saida){
        this.saidas.add(saida);
    }

}
