package briatore.financeiro.service;

import briatore.financeiro.entity.Pagamento;
import briatore.financeiro.repository.PagamentoRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class PagamentoService {

    PagamentoRepository pagamentoRepository;

    public PagamentoService(PagamentoRepository pagamentoRepository) {
        this.pagamentoRepository = pagamentoRepository;
    }

    public Pagamento processarPagamento(String responsavel,
                                        String numeroCartao,
                                        BigDecimal valor) {
        //TODO: verificar se o cliente possui crédito suficiente
        Pagamento pagamento = new Pagamento(responsavel, numeroCartao, valor);
        return this.pagamentoRepository.save(pagamento);
    }

    public void cancelarPagamento(Pagamento pagamento) {
        pagamento.setStatus(Pagamento.STATUS_CANCELADO);
        this.pagamentoRepository.save(pagamento);
    }
}
