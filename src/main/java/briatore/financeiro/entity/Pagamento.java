package briatore.financeiro.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import java.math.BigDecimal;

@Entity
public class Pagamento {

	public static final String STATUS_PROCESSADO = "PROCESSADO";
	public static final String STATUS_CANCELADO = "CANCELADO";
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String responsavel;
	private String numeroCartao;
	private BigDecimal valor;
	private String status;

	public Pagamento() {
	}

	public Pagamento(final String responsavel, final String numeroCartao, final BigDecimal valor) {
		this.responsavel = responsavel;
		this.numeroCartao = numeroCartao;
		this.status = STATUS_PROCESSADO;
		this.valor = valor;
	}

	public String getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}

	public String getNumeroCartao() {
		return numeroCartao;
	}

	public void setNumeroCartao(String numeroCartao) {
		this.numeroCartao = numeroCartao;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	public void setValor(final BigDecimal valor) {
		this.valor = valor;
	}

	public void setId(final Integer id) {
		this.id = id;
	}

	public String getStatus() {
		return this.status;
	}

	public BigDecimal getValor() {
		return this.valor;
	}

	public Integer getId() {
		return this.id;
	}

	public boolean isProcessado() {
		return STATUS_PROCESSADO.equals(this.status);
	}
	
	public boolean isCancelado() {
		return STATUS_CANCELADO.equals(this.status);
	}

	@Override
	public String toString() {
		return "Pagamento [id = " + this.id + ", status = " + this.status + ", valor = " + this.valor +  "]";
	}
}
