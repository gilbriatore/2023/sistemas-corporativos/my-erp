package briatore.financeiro.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MenuFinanceiroView {
	@GetMapping("/financeiro")
	public String navegarPara() {
		return "/financeiro/menu-financeiro";
	}
}
