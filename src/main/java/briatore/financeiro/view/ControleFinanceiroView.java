package briatore.financeiro.view;

import briatore.estoque.entity.Entrada;
import briatore.estoque.entity.ItemEstoque;
import briatore.estoque.entity.Produto;
import briatore.estoque.repository.EntradaRepository;
import briatore.estoque.repository.ItemEstoqueRepository;
import briatore.estoque.repository.ProdutoRepository;
import briatore.financeiro.entity.Pagamento;
import briatore.financeiro.repository.PagamentoRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
import java.util.Optional;

@Controller
public class ControleFinanceiroView {

	PagamentoRepository pagamentoRepository;

	public ControleFinanceiroView(PagamentoRepository pagamentoRepository) {
		this.pagamentoRepository = pagamentoRepository;
	}

	@GetMapping("/financeiro/pagamentos")
	public String navegarPara(Model model) {
		List<Pagamento> listaPagamentos = pagamentoRepository.findAll();
		model.addAttribute("listaPagamentos", listaPagamentos);
		return "/financeiro/lista-de-pagamentos";
	}

}
